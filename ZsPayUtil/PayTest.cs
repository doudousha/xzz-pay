﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using PayHelper;

namespace ZsPayUtil
{
    [TestFixture]
    public class PayTest
    {
        private string _paySecret = System.Configuration.ConfigurationSettings.AppSettings["paySecret"];
        private string _payKey = System.Configuration.ConfigurationSettings.AppSettings["payKey"];
        private string _payQueryUrl = System.Configuration.ConfigurationSettings.AppSettings["payQueryUrl"];
        private string _quickPayGateWay = System.Configuration.ConfigurationSettings.AppSettings["quickPayGateWay"];
        private string _orderIp = System.Configuration.ConfigurationSettings.AppSettings["orderIp"];
        private string _returnUrl = System.Configuration.ConfigurationSettings.AppSettings["returnUrl"];
        private string _notifyUrl = System.Configuration.ConfigurationSettings.AppSettings["notifyUrl"];


        [Test]
        public void GetSign()
        {
            SortedDictionary<string, string> paramMap = new SortedDictionary<string, string>();
            paramMap.Add("payKey", _payKey);
            paramMap.Add("outTradeNo", ""); // 原交易订单号
        }

        [Test]
        public void pay()
        {
            SortedDictionary<string, string> paramMap = new SortedDictionary<string, string>();
            paramMap.Add("payKey", _payKey);
            paramMap.Add("outTradeNo", "10011"); // 原交易订单号
            paramMap.Add("sign", MerchantApiUtil.GetSign(paramMap, _paySecret));

            // 创建http请求
            JObject jsonResult = SimpleHttpUtils.PostToJson(_payQueryUrl, paramMap);
            string resultCode = (string) jsonResult["resultCode"]; // 返回码
            string errMsg = (string) jsonResult["errMsg"]; //错误信息(请求失败时)
            if ("0000".Equals(resultCode.ToString()))
            {
                //请求成功
                Console.WriteLine("请求成功");
            }
            else
            {
                //请求失败
                Console.WriteLine(errMsg);
            }
        }

        [Test]
        public void QuickGatewayPayApplyH5()
        {
            string outTradeNo = DateTime.Now.ToString("yyyyMMddHHmmss");
            QuickGatewayPayApplyH5(outTradeNo);
        }

        [Test]
        public void SignCheck()
        {
            SortedDictionary<string, string> paramMap = new SortedDictionary<string, string>();
            String paySecret = "ce6287d8fc7c45b4aed2c3d64a822c86";
            /*JSON字符串不需要按照顺序排列参数，因为排序工作已经封装，实际上制作签名还是需要排序，这里只是提供检验
			注意不要将sign字段和paySecret字段也放入JSON
		*/
            String text = "{'orderTime':'20170808151421',"
                          + "'trxNo':'TES77772017080810002065',"
                          + "'outTradeNo':'1502176461184',"
                          + "'successTime':'20170808151443',"
                          + "'tradeStatus':'SUCCESS',"
                          + "'orderPrice':'0.01',"
                          + "'payKey':'0784faa976d9461e9663163bd4f49953',"
                          + "'remark':'remark','productName':"
                          + "'testproduct',"
                          + "'productType':'10000103'"
                          + "}";

            //转换成JSON对象，并set入Map中，内部会进行排序
            JObject jsonObject = JObject.Parse(text);
            foreach (var item in jsonObject)
            {
                paramMap.Add(item.Key, item.Value.ToString());
            }
            //制作签名
            string sign = MerchantApiUtil.GetSign(paramMap, paySecret);
        }

        private void QuickGatewayPayApplyH5(string outTradeNo)
        {

            SortedDictionary<string, string> paramMap = new SortedDictionary<string, string>();
            paramMap.Add("productType", "40000503"); //40000501 T1快捷支付  40000502  D0快捷  40000503 T0快捷支付
            paramMap.Add("payKey", _payKey); // 商户支付Key
            paramMap.Add("orderPrice", "0.21");

            paramMap.Add("payBankAccountNo", ""); //支付银行卡
            paramMap.Add("payPhoneNo", ""); //手机号码
            paramMap.Add("payBankAccountName", ""); //开户人姓名
            paramMap.Add("payCertNo", ""); //身份证号码

            paramMap.Add("outTradeNo", outTradeNo);
            paramMap.Add("productName", "纸巾"); // 商品名称
            paramMap.Add("orderIp", _orderIp); // 下单IP
           // paramMap.Add("orderTime", DateTime.Now.ToString("yyyyMMddHHmmss")); // 订单时间
            paramMap.Add("orderTime", "20180403142210"); // 订单时间
            paramMap.Add("returnUrl", _returnUrl); // 页面通知返回url
            paramMap.Add("notifyUrl", _notifyUrl); // 后台消息通知Url
            paramMap.Add("remark", "支付备注");
            paramMap.Add("sign", MerchantApiUtil.GetSign(paramMap, _paySecret));
            String payResult = SimpleHttpUtils.Post(_quickPayGateWay, paramMap);
            Console.WriteLine(payResult);
        }
    }
}
