﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayHelper
{
    public class MerchantApiUtil
    {
        public static string GetSign(SortedDictionary<string, string> paramMap, string pauySecret)
        {
           
            string _paySecret = System.Configuration.ConfigurationSettings.AppSettings["paySecret"];
            StringBuilder builder = new StringBuilder();
            foreach (var item in paramMap)
            {
                if (item.Key != null && string.IsNullOrEmpty(item.Value) == false)
                {
                    Console.WriteLine("签名:" + item.Key);
                    builder.Append(item.Key).Append("=").Append(item.Value).Append("&");
                }
            }
            builder.Remove(builder.Length - 1, 1);
            string argPreSign = builder.Append("&paySecret=").Append(_paySecret).ToString();
            string signStr = MD5Util.encode(argPreSign).ToUpper();
            return signStr;
        }
    }
}