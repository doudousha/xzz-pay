﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json.Linq;

namespace PayHelper
{
    public class SimpleHttpUtils
    {
        public static string Post(string url, SortedDictionary<string, string> postParams)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded; charset=utf-8";

            if (postParams != null && postParams.Count > 0)
            {
                string paramStr = DictToString(postParams);
                byte[] byteArray = Encoding.UTF8.GetBytes(paramStr);
                request.ContentLength = byteArray.Length;
                var datastream = request.GetRequestStream();
                datastream.Write(byteArray, 0, byteArray.Length);
                datastream.Close();
            }
            HttpWebResponse resp = (HttpWebResponse)request.GetResponse();
            Stream stream = resp.GetResponseStream();

            string result = null;
            //获取内容  
            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
            {
                result = reader.ReadToEnd();
            }
            return result;
        }


        public static string DictToString(SortedDictionary<string, string> dict)
        {
            StringBuilder builder = new StringBuilder();
            foreach (var  item in dict)
            {
                 if (string.IsNullOrEmpty(item.Value) == false)
                {
                    builder.Append(item.Key + "=" + item.Value + "&");
                }
            }
            string resultStr = builder.ToString();
            return resultStr;
        }

        public static JObject PostToJson(string url, SortedDictionary<string, string> postParams)
        {
            string resposneContent = Post(url, postParams);
            JObject json = JObject.Parse(resposneContent);
            return json;
        }
    }
}